package errors

import "errors"

var ErrMissingAWSAccessKeyID = errors.New("missing value OF AWS_ACCESS_KEY_ID")

var ErrMissingAWSRegion = errors.New("missing value OF AWS_REGION")

var ErrMissingAWSSecretAccessKey = errors.New("missing value OF AWS_SECRECT_ACCESS_KEY")

var ErrMissingGCPServiceAccountKeyJSON = errors.New("missing value OF GCP_SERVICE_ACCOUNT_KEY_JSON")

var ErrMissingGCPType = errors.New("missing value OF GCP_TYPE")

var ErrMissingGCPProjectID = errors.New("missing value OF GCP_PROJECT_ID")

var ErrMissingGCPPrivateKeyID = errors.New("missing value OF GCP_PRIVATE_KEY_ID")

var ErrMissingGCPPrivateKey = errors.New("missing value OF GCP_PRIVATE_KEY")

var ErrMissingGCPClientEmail = errors.New("missing value OF GCP_CLIENT_EMAIL")

var ErrMissingGCPClientID = errors.New("missing value OF GCP_CLIENT_ID")

var ErrMissingGCPAuthURI = errors.New("missing value OF GCP_AUTH_URI")

var ErrMissingGCPTokenURI = errors.New("missing value OF GCP_TOKEN_URI")

var ErrMissingGCPAuthProviderX509CertURL = errors.New("missing value OF GCP_AUTH_PROVIDER_X509_CERT_URL")

var ErrMissingGCPClientX509CertURL = errors.New("missing value OF GCP_CLIENT_X509_CERT_URL")

var ErrMissingGCPBucketName = errors.New("missing value OF GCP_BUCKET_NAME")

var ErrMissingGCPAuthProviderURL = errors.New("missing value OF GCP_AUTH_PROVIDER_URL")

var ErrMissingGCPClientCertURL = errors.New("missing value OF GCP_CLIENT_CERT_URL")

var ErrInvalidCloudProvider = errors.New("invalid value OF CLOUD_PROVIDER, valid values are 'AWS' and 'GCP'")

var ErrDeadlineExceeded = errors.New("rpc error: code = DeadlineExceeded desc = context deadline exceeded")
