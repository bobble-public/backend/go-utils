package config

import (
	"strings"

	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
	"gitlab.com/bobble-public/backend/go-utils/cloudservices/errors"
	"gitlab.com/bobble-public/backend/go-utils/logging"
)

type PermissionScope string

const (
	// View and manage your data across Google Cloud Platform services
	CloudPlatformScope PermissionScope = "https://www.googleapis.com/auth/cloud-platform"

	// View your data across Google Cloud Platform services
	CloudPlatformReadOnlyScope PermissionScope = "https://www.googleapis.com/auth/cloud-platform.read-only"

	// Manage your data and permissions in Google Cloud Storage
	DevstorageFullControlScope PermissionScope = "https://www.googleapis.com/auth/devstorage.full_control"

	// View your data in Google Cloud Storage
	DevstorageReadOnlyScope PermissionScope = "https://www.googleapis.com/auth/devstorage.read_only"

	// Manage your data in Google Cloud Storage
	DevstorageReadWriteScope PermissionScope = "https://www.googleapis.com/auth/devstorage.read_write"
)

func (c PermissionScope) String() string {
	return string(c)
}

func (c PermissionScope) IsValid() bool {
	switch c {
	case CloudPlatformScope, CloudPlatformReadOnlyScope, DevstorageFullControlScope, DevstorageReadOnlyScope, DevstorageReadWriteScope:
		return true
	default:
		return false
	}
}

// GCPServiceAccountCredentials represents the GCS configuration
type GCPServiceAccountCredentials struct {
	Type            string `json:"type" mapstructure:"type"`
	ProjectID       string `json:"project_id" mapstructure:"project_id"`
	PrivateKeyID    string `json:"private_key_id" mapstructure:"private_key_id"`
	PrivateKey      string `json:"private_key" mapstructure:"private_key"`
	ClientEmail     string `json:"client_email" mapstructure:"client_email"`
	ClientID        string `json:"client_id" mapstructure:"client_id"`
	AuthURI         string `json:"auth_uri" mapstructure:"auth_uri"`
	TokenURI        string `json:"token_uri" mapstructure:"token_uri"`
	AuthProviderURL string `json:"auth_provider_x509_cert_url" mapstructure:"auth_provider_x509_cert_url"`
	ClientCertURL   string `json:"client_x509_cert_url" mapstructure:"client_x509_cert_url"`
}

// AWSCredentials represents the AWS configuration
type AWSCredentials struct {
	AccessKeyID     string
	SecretAccessKey string
	Region          string
}

// CloudProvider is the cloud provider name , default is AWS
var CloudProvider = CloudProviderAWS

const (
	// CloudProviderAWS is the cloud provider name for AWS
	CloudProviderAWS = "AWS"

	// CloudProviderGCP is the cloud provider name for GCP
	CloudProviderGCP = "GCP"
)

// GCPTokenSource is the token source for GCP
var GCPTokenSource string

// SetCloudProviderName sets the cloud provider name
func SetCloudProviderName() {

	// If the default cloud provider is not set, then we are using default cloud provider
	if !viper.IsSet("CLOUD_PROVIDER") || viper.GetString("CLOUD_PROVIDER") == "" {
		return
	}
	// If the cloud provider is set, then we are using the cloud provider set by the user
	cloudProvider := viper.GetString("CLOUD_PROVIDER")
	switch cloudProvider {
	case CloudProviderAWS:
		CloudProvider = CloudProviderAWS
	case CloudProviderGCP:
		CloudProvider = CloudProviderGCP
	default:
		// If the cloud provider is not AWS or GCP, then throw an error
		logging.Fatal(errors.ErrInvalidCloudProvider.Error())
	}
}

func GetAWSCredentials() (credentials AWSCredentials) {

	credentials.AccessKeyID = viper.GetString("AWS_ACCESS_KEY_ID")
	credentials.SecretAccessKey = viper.GetString("AWS_SECRET_ACCESS_KEY")
	credentials.Region = viper.GetString("AWS_REGION")

	if len(credentials.AccessKeyID) == 0 {
		logging.Fatal(errors.ErrMissingAWSAccessKeyID.Error())
	} else if len(credentials.Region) == 0 {
		logging.Fatal(errors.ErrMissingAWSRegion.Error())
	} else if len(credentials.SecretAccessKey) == 0 {
		logging.Fatal(errors.ErrMissingAWSSecretAccessKey.Error())
	}

	return
}

func GetGCPServiceAccountCredentials() (credentials GCPServiceAccountCredentials) {

	GCPTokenSource = viper.GetString("GCP_TOKEN_SOURCE")

	if strings.ToLower(viper.GetString("GCP_TOKEN_SOURCE")) != "custom" {
		return
	}

	if !viper.IsSet("GCP_SERVICE_ACCOUNT_KEY_JSON") {
		logging.Fatal(errors.ErrMissingGCPServiceAccountKeyJSON.Error())
	}

	gcpCredentials := viper.GetStringMapString("GCP_SERVICE_ACCOUNT_KEY_JSON")

	err := mapstructure.Decode(gcpCredentials, &credentials)
	if err != nil {
		logging.Fatal(err.Error())
	}

	if len(credentials.Type) == 0 {
		logging.Fatal(errors.ErrMissingGCPType.Error())
	} else if len(credentials.ProjectID) == 0 {
		logging.Fatal(errors.ErrMissingGCPProjectID.Error())
	} else if len(credentials.PrivateKeyID) == 0 {
		logging.Fatal(errors.ErrMissingGCPPrivateKeyID.Error())
	} else if len(credentials.PrivateKey) == 0 {
		logging.Fatal(errors.ErrMissingGCPPrivateKey.Error())
	} else if len(credentials.ClientEmail) == 0 {
		logging.Fatal(errors.ErrMissingGCPClientEmail.Error())
	} else if len(credentials.ClientID) == 0 {
		logging.Fatal(errors.ErrMissingGCPClientID.Error())
	} else if len(credentials.AuthURI) == 0 {
		logging.Fatal(errors.ErrMissingGCPAuthURI.Error())
	} else if len(credentials.TokenURI) == 0 {
		logging.Fatal(errors.ErrMissingGCPTokenURI.Error())
	} else if len(credentials.AuthProviderURL) == 0 {
		logging.Fatal(errors.ErrMissingGCPAuthProviderURL.Error())
	} else if len(credentials.ClientCertURL) == 0 {
		logging.Fatal(errors.ErrMissingGCPClientCertURL.Error())
	}
	return
}
