package queueservice

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"time"

	googlePubsub "cloud.google.com/go/pubsub"
	raw "cloud.google.com/go/pubsub/apiv1"
	pubsubpb "cloud.google.com/go/pubsub/apiv1/pubsubpb"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"gitlab.com/bobble-public/backend/go-utils/cloudservices/config"
	cloudserviceErrors "gitlab.com/bobble-public/backend/go-utils/cloudservices/errors"
	"gitlab.com/bobble-public/backend/go-utils/logging"
	promutil "gitlab.com/bobble-public/backend/go-utils/promutil"
	"gocloud.dev/gcp"
	"gocloud.dev/pubsub"
	"gocloud.dev/pubsub/awssnssqs"
	"gocloud.dev/pubsub/driver"
	"gocloud.dev/pubsub/gcppubsub"
	"golang.org/x/oauth2/google"
	"google.golang.org/grpc"
)

// ErrNoQueueMessage is an error object for empty queue
var ErrNoQueueMessage = errors.New("0 messages received from queue")

// base64EncodedKey is the Message Attribute key used to flag that the
// message body is base64 encoded.
var base64EncodedKey = "base64encoded"

// BodyBase64Encoding is an enum of strategies for when to base64 message
// bodies.
type BodyBase64Encoding int

// Queue provides the interface for Queue Service
type Queue interface {
	Shutdown(ctx context.Context) error
	SendMessage(ctx context.Context, message string, metadata *map[string]string) error
	ReceiveMessage() (*driver.Message, error)
	DeleteMessage(ctx context.Context, msg *driver.Message) error
	SendMessages(ctx context.Context, msgs []*driver.Message) error
	ReceiveMessages(maxMessages int) (msgs []*driver.Message, err error)
	DeleteMessages(ctx context.Context, msgs []*driver.Message) error
	ModifyAckDeadline(ctx context.Context, msgs []*driver.Message, ackDeadlineSeconds int64) error
}

func (queue *queue) Shutdown(ctx context.Context) error {

	// Close queue
	if queue.subscription != nil {
		if err := queue.subscription.Shutdown(ctx); err != nil {
			return err
		}
	}

	// Close topic
	if queue.topic != nil {
		if err := queue.topic.Shutdown(ctx); err != nil {
			return err
		}
	}

	// Close the publisher client.
	if queue.pubClient != nil {
		err := queue.pubClient.Close()
		if err != nil {
			return err
		}
	}

	// Close the subscription client.
	if queue.subClient != nil {
		err := queue.subClient.Close()
		if err != nil {
			return err
		}
	}

	// Close the gRPC connection.
	if queue.cleanup != nil {
		defer queue.cleanup()
	}

	// Log shutdown
	var logFields string
	if queue.topicName != "" {
		logFields = "topicName: " + queue.topicName
	}
	if queue.subscriptionName != "" {
		logFields += " subscriptionName: " + queue.subscriptionName
	}
	logging.WithFields(logging.Fields{"queue ": logFields}).Info("queue shutdown")

	return nil
}

type queue struct {
	topicName        string
	subscriptionName string
	topic            *pubsub.Topic
	subscription     *pubsub.Subscription
	cleanup          func()
	pubClient        *raw.PublisherClient
	subClient        *raw.SubscriberClient
	queueOptions     *QueueOptions
}

// QueueOptions provides the options for queue service
type QueueOptions struct {
	// WaitTime passed to ReceiveMessage to enable long polling.
	// https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-short-and-long-polling.html#sqs-long-polling.
	// Note that a non-zero WaitTime can delay delivery of messages
	// by up to that duration.
	ReceiverWaitTime time.Duration
	//    * VisibilityTimeout – The visibility timeout for the queue, in seconds.
	//    Valid values: An integer from 0 to 43,200 (12 hours). Default: 30. For
	//    more information about the visibility timeout, see Visibility Timeout
	//    (https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-visibility-timeout.html)
	//    in the Amazon SQS Developer Guide.
	// The duration (in seconds) that the received messages are hidden from subsequent
	// retrieve requests after being retrieved by a ReceiveMessage request.
	ReceiverVisibilityTimeout *time.Duration

	// CloudProvider is the cloud provider for queue service
	CloudProvider string
}

// NewQueue returns a new instance of queue service
func NewQueue(ctx context.Context, topicName *string, subscriptionName *string, queueOptions *QueueOptions) Queue {
	var topic *pubsub.Topic
	var subscription *pubsub.Subscription
	var pubClient *raw.PublisherClient
	var subClient *raw.SubscriberClient
	var cleanup func()

	if topicName == nil && subscriptionName == nil {
		logging.Fatal("topic name and subscription name cannot be nil")
	}

	if queueOptions == nil {
		queueOptions = &QueueOptions{}
	}

	var cloudProvider = config.CloudProvider
	// If custom cloud provider for queue is provided
	if queueOptions.CloudProvider != "" {
		cloudProvider = queueOptions.CloudProvider
	}

	switch cloudProvider {
	case config.CloudProviderGCP:
		gcpServiceAccountCredentials := config.GetGCPServiceAccountCredentials()
		var creds *google.Credentials
		var err error

		if config.GCPTokenSource == "custom" {
			jsonKey, err := json.Marshal(gcpServiceAccountCredentials)
			if err != nil {
				logging.Fatal("failed to marshal GCP config: " + err.Error())
			}
			// Load the credentials from the JSON key
			creds, err = google.CredentialsFromJSON(ctx, jsonKey, googlePubsub.ScopePubSub)
			if err != nil {
				logging.Fatal("failed to load GCP credentials from JSON key file: " + err.Error())
			}
		} else {
			// Load the default GCP credentials with Cloud Platform scope.
			creds, err = gcp.DefaultCredentials(context.Background())
			if err != nil {
				logging.Fatal("failed to load Default GCP credentials: " + err.Error())
			}
		}

		var conn *grpc.ClientConn
		conn, cleanup, err = gcppubsub.Dial(ctx, creds.TokenSource)
		if err != nil {
			logging.Fatal("failed to dial GCP pubsub: " + err.Error())
		}
		if topicName != nil {
			// Create a publisher client.
			pubClient, err = gcppubsub.PublisherClient(ctx, conn)
			if err != nil {
				logging.Fatal("failed to create GCP pubsub client: " + err.Error())
			}

			// Create a topic options object.

			topic, err = gcppubsub.OpenTopicByPath(pubClient, *topicName, nil)
			if err != nil {
				logging.Fatal("failed to open GCP pubsub topic: " + err.Error())
			}
		}
		if subscriptionName != nil {
			// Create a new subscription to the topic.
			subClient, err = gcppubsub.SubscriberClient(ctx, conn)
			if err != nil {
				logging.Fatal("failed to create GCP pubsub client: " + err.Error())
			}

			// Create a subscription options object.

			// Open the connection to the subscription.
			subscription, err = gcppubsub.OpenSubscriptionByPath(subClient, *subscriptionName, nil)
			if err != nil {
				logging.Fatal("failed to open GCP pubsub subscription: " + err.Error())
			}
		}
	case config.CloudProviderAWS:
		awsCredentials := config.GetAWSCredentials()
		accessKeyID := awsCredentials.AccessKeyID
		secretAccessKey := awsCredentials.SecretAccessKey
		region := awsCredentials.Region

		sqsSession, err := session.NewSession(
			&aws.Config{
				Region: aws.String(region),
				Credentials: credentials.NewStaticCredentials(
					accessKeyID,
					secretAccessKey,
					"",
				),
			})
		if err != nil {
			logging.Fatal("failed to create AWS session: " + err.Error())
		}

		// Create a topic options object.
		if topicName != nil {
			// Open the session to the topic.
			topic = awssnssqs.OpenSQSTopic(ctx, sqsSession, *topicName, nil)
		}
		// Create a subscription options object.
		if subscriptionName != nil {
			// Open the session to the subscription.
			subscription = awssnssqs.OpenSubscription(ctx, sqsSession, *subscriptionName, nil)
		}
	default:
		logging.Fatal(cloudserviceErrors.ErrInvalidCloudProvider.Error())
	}

	queue := queue{cleanup: cleanup, queueOptions: queueOptions}

	if topicName != nil {
		queue.topicName = *topicName
		queue.topic = topic
		queue.pubClient = pubClient
	}

	if subscriptionName != nil {
		queue.subscriptionName = *subscriptionName
		queue.subscription = subscription
		queue.subClient = subClient
	}

	return &queue
}

// ReceiveMessage receives the message from SQS/Pubsub
func (queue *queue) ReceiveMessage() (*driver.Message, error) {

	msg, err := queue.ReceiveMessages(1)
	if err != nil {
		return nil, err
	}

	if len(msg) == 0 {
		return nil, ErrNoQueueMessage
	}
	logging.WithFields(logging.Fields{"topicName": queue.topicName}).Debug("queue.ReceiveMessage: msg AckID: " + fmt.Sprint(msg[0].AckID))

	return msg[0], nil
}

// SendMessage pushes a new message to SQS/Pubsub
func (queue *queue) SendMessage(ctx context.Context, message string, metadata *map[string]string) error {

	logging.WithFields(logging.Fields{"topicName": queue.topicName}).Debug("queue.SendMessage: message: " + message)
	start := time.Now()

	if metadata == nil {
		metadata = &map[string]string{}
	}

	err := queue.SendMessages(ctx, []*driver.Message{{Body: []byte(message), Metadata: *metadata}})

	status := "success"
	if err != nil {
		status = "error"
		logging.WithFields(logging.Fields{"topicName": queue.topicName}).Error("queue.SendMessage: err: " + err.Error())
	}
	promutil.GetQueueProducerRecorder().Record(queue.topicName, status, time.Since(start).Seconds())

	return err
}

// DeleteMessage deletes a message from SQS/Pubsub
func (queue *queue) DeleteMessage(ctx context.Context, msg *driver.Message) error {

	err := queue.DeleteMessages(ctx, []*driver.Message{msg})
	if err != nil {
		logging.WithFields(logging.Fields{"topicName": queue.topicName}).Error("queue.DeleteMessage: err: " + err.Error())
		return err
	}
	return nil
}

// ReceiveMessages receives a batch of messages from SQS/Pubsub
func (queue *queue) ReceiveMessages(maxMessages int) (msgs []*driver.Message, err error) {

	if maxMessages == 0 {
		return nil, errors.New("maxMessages cannot be 0")
	}

	ctx := context.Background()

	if queue.queueOptions.ReceiverWaitTime != 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, queue.queueOptions.ReceiverWaitTime)
		defer cancel()
	}

	var sqsClient *sqs.SQS
	var sc *raw.SubscriberClient

	if queue.subscription.As(&sqsClient) {
		req := &sqs.ReceiveMessageInput{
			QueueUrl:              aws.String(queue.subscriptionName),
			MaxNumberOfMessages:   aws.Int64(int64(maxMessages)),
			MessageAttributeNames: []*string{aws.String("All")},
			AttributeNames:        []*string{aws.String("All")},
		}
		if queue.queueOptions.ReceiverWaitTime != 0 {
			req.WaitTimeSeconds = aws.Int64(int64(queue.queueOptions.ReceiverWaitTime.Seconds()))
		}
		if queue.queueOptions.ReceiverVisibilityTimeout != nil {
			req.VisibilityTimeout = aws.Int64(int64(queue.queueOptions.ReceiverVisibilityTimeout.Seconds()))
		}
		output, err := sqsClient.ReceiveMessage(req)
		if err != nil {
			return nil, err
		}
		for _, message := range output.Messages {
			bodyStr := aws.StringValue(message.Body)
			rawAttrs := map[string]string{}
			for k, v := range message.MessageAttributes {
				rawAttrs[k] = aws.StringValue(v.StringValue)
			}
			attrs := map[string]string{}
			decodeIt := false
			for k, v := range rawAttrs {
				// See BodyBase64Encoding for details on when we base64 decode message bodies.
				if k == base64EncodedKey {
					decodeIt = true
					continue
				}
				// See the package comments for more details on escaping of metadata
				// keys & values.
				attrs[HexUnescape(k)] = URLUnescape(v)
			}
			var b []byte
			if decodeIt {
				var err error
				b, err = base64.StdEncoding.DecodeString(bodyStr)
				if err != nil {
					// Fall back to using the raw message.
					b = []byte(bodyStr)
				}
			} else {
				b = []byte(bodyStr)
			}
			msg := &driver.Message{
				LoggableID: aws.StringValue(message.MessageId),
				Body:       b,
				Metadata:   attrs,
				AckID:      message.ReceiptHandle,
			}
			msgs = append(msgs, msg)
		}
	} else if queue.subscription.As(&sc) {
		req := &pubsubpb.PullRequest{
			Subscription: queue.subscriptionName,
			MaxMessages:  int32(maxMessages),
		}
		resp, err := sc.Pull(ctx, req)

		if err != nil {
			if err.Error() == cloudserviceErrors.ErrDeadlineExceeded.Error() {
				return nil, ErrNoQueueMessage
			}
			return nil, err
		}

		// sc.Pull returns an empty list if there are no messages available in the
		// backlog. We should skip processing steps when that happens.
		if len(resp.ReceivedMessages) == 0 {
			return nil, nil
		}

		ackIDs := []string{}
		for _, rm := range resp.ReceivedMessages {
			rm := rm
			rmm := rm.Message
			m := &driver.Message{
				LoggableID: rmm.MessageId,
				Body:       rmm.Data,
				Metadata:   rmm.Attributes,
				AckID:      rm.AckId,
			}
			msgs = append(msgs, m)
			ackIDs = append(ackIDs, rm.AckId)
		}

		// If the queue has a visibility timeout, modify the AckDeadline for the received messages.
		// Applicable to same subscription only.
		if queue.queueOptions.ReceiverVisibilityTimeout != nil {
			// Modify the AckDeadline for the received messages.
			reqModify := &pubsubpb.ModifyAckDeadlineRequest{
				Subscription:       queue.subscriptionName,
				AckIds:             ackIDs,
				AckDeadlineSeconds: int32(queue.queueOptions.ReceiverVisibilityTimeout.Seconds()),
			}
			if err := sc.ModifyAckDeadline(ctx, reqModify); err != nil {
				return nil, err
			}
		}
	}
	return
}

// SendMessages sends a batch of messages to SQS/Pubsub
func (queue *queue) SendMessages(ctx context.Context, msgs []*driver.Message) error {

	if len(msgs) == 0 {
		return errors.New("no messages to send")
	}

	var sqsClient *sqs.SQS
	var pc *raw.PublisherClient

	// AWS Queue batch implementation
	if queue.topic.As(&sqsClient) {
		req := &sqs.SendMessageBatchInput{
			QueueUrl: aws.String(queue.topicName),
		}
		for _, dm := range msgs {
			attrs := map[string]*sqs.MessageAttributeValue{}
			for k, v := range encodeMetadata(dm.Metadata) {
				attrs[k] = &sqs.MessageAttributeValue{
					DataType:    aws.String("String"),
					StringValue: aws.String(v),
				}
			}
			if len(attrs) == 0 {
				attrs = nil
			}
			entry := &sqs.SendMessageBatchRequestEntry{
				Id:                aws.String(strconv.Itoa(len(req.Entries))),
				MessageAttributes: attrs,
				MessageBody:       aws.String(string(dm.Body)),
			}
			req.Entries = append(req.Entries, entry)
		}
		resp, err := sqsClient.SendMessageBatchWithContext(ctx, req)
		if err != nil {
			return err
		}
		if numFailed := len(resp.Failed); numFailed > 0 {
			first := resp.Failed[0]
			return awserr.New(aws.StringValue(first.Code), fmt.Sprintf("sqs.SendMessageBatch failed for %d message(s): %s", numFailed, aws.StringValue(first.Message)), nil)
		}
	} else if queue.topic.As(&pc) {
		// GCP Pubsub batch implementation
		var ms []*pubsubpb.PubsubMessage
		for _, dm := range msgs {
			psm := &pubsubpb.PubsubMessage{Data: dm.Body, Attributes: dm.Metadata}
			ms = append(ms, psm)
		}
		req := &pubsubpb.PublishRequest{Topic: queue.topicName, Messages: ms}
		_, err := pc.Publish(ctx, req)
		if err != nil {
			return err
		}
	}

	return nil
}

// DeleteMessages sends a batch of acks to SQS/Pubsub
func (queue *queue) DeleteMessages(ctx context.Context, msgs []*driver.Message) error {

	ids := []driver.AckID{}
	for _, msg := range msgs {
		ids = append(ids, msg.AckID)
	}

	if len(ids) == 0 {
		return errors.New("no messages to delete")
	}

	var sqsClient *sqs.SQS
	var pc *raw.SubscriberClient

	// AWS Queue batch ack implementation
	if queue.subscription.As(&sqsClient) {
		req := &sqs.DeleteMessageBatchInput{QueueUrl: aws.String(queue.subscriptionName)}
		for _, id := range ids {
			req.Entries = append(req.Entries, &sqs.DeleteMessageBatchRequestEntry{
				Id:            aws.String(strconv.Itoa(len(req.Entries))),
				ReceiptHandle: id.(*string),
			})
		}
		resp, err := sqsClient.DeleteMessageBatchWithContext(ctx, req)
		if err != nil {
			return err
		}
		// Note: DeleteMessageBatch doesn't return failures when you try
		// to Delete an id that isn't found.
		if numFailed := len(resp.Failed); numFailed > 0 {
			first := resp.Failed[0]
			return awserr.New(aws.StringValue(first.Code), fmt.Sprintf("sqs.DeleteMessageBatch failed for %d message(s): %s", numFailed, aws.StringValue(first.Message)), nil)
		}
	} else if queue.subscription.As(&pc) {
		ackIds := make([]string, 0, len(ids))
		for _, id := range ids {
			ackIds = append(ackIds, id.(string))
		}
		return pc.Acknowledge(ctx, &pubsubpb.AcknowledgeRequest{Subscription: queue.subscriptionName, AckIds: ackIds})
	}
	return nil
}

// ModifyAckDeadline modifies the acknowledgment deadline for a batch of messages.
//
// This function takes a context, a slice of messages, and the new acknowledgment
// deadline in seconds. It checks for empty messages and nil AckIDs to ensure
// robust error handling. If the queue client is valid, it updates the visibility
// timeout for SQS or modifies the acknowledgment deadline for Pub/Sub messages.
//
// Parameters:
// - ctx: The context for managing cancellation and timeouts.
// - msgs: A slice of messages for which the acknowledgment deadline needs to be modified.
// - ackDeadlineSeconds: The new acknowledgment deadline in seconds.
//
// Returns:
//   - error: Returns an error if the messages slice is empty, if any AckID is nil,
//     or if no valid queue client is found for modifying the acknowledgment deadline.
func (queue *queue) ModifyAckDeadline(ctx context.Context, msgs []*driver.Message, ackDeadlineSeconds int64) error {
	// Check for empty messages
	if len(msgs) == 0 {
		return errors.New("no messages provided to modify ack deadline")
	}

	var sqsClient *sqs.SQS
	if queue.subscription.As(&sqsClient) {
		entries := make([]*sqs.ChangeMessageVisibilityBatchRequestEntry, len(msgs))
		for i, msg := range msgs {
			// Check for nil AckID
			if msg.AckID == nil {
				return errors.New("message AckID is nil")
			}
			entries[i] = &sqs.ChangeMessageVisibilityBatchRequestEntry{
				Id:                aws.String(strconv.Itoa(i)),
				ReceiptHandle:     msg.AckID.(*string),
				VisibilityTimeout: aws.Int64(ackDeadlineSeconds),
			}
		}
		_, err := sqsClient.ChangeMessageVisibilityBatch(&sqs.ChangeMessageVisibilityBatchInput{
			QueueUrl: aws.String(queue.subscriptionName),
			Entries:  entries,
		})
		return err
	}
	var sc *raw.SubscriberClient
	if queue.subscription.As(&sc) {
		ackIDs := make([]string, len(msgs))
		for i, msg := range msgs {
			// Check for nil AckID
			if msg.AckID == nil {
				return errors.New("message AckID is nil")
			}
			ackIDs[i] = msg.AckID.(string)
		}
		return sc.ModifyAckDeadline(ctx, &pubsubpb.ModifyAckDeadlineRequest{Subscription: queue.subscriptionName, AckIds: ackIDs, AckDeadlineSeconds: int32(ackDeadlineSeconds)})
	}
	return fmt.Errorf("no valid queue client found for modifying ack deadline")
}
