package bigqueryservice

import (
	"context"
	"encoding/json"

	"cloud.google.com/go/bigquery"
	"gitlab.com/bobble-public/backend/go-utils/cloudservices/config"
	"gitlab.com/bobble-public/backend/go-utils/logging"
	"gocloud.dev/gcp"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/option"
)

// NewClient creates an instance of BigQuery client
func NewClient(ctx context.Context, projectId *string) (client *bigquery.Client, err error) {

	gcpServiceAccountCredentials := config.GetGCPServiceAccountCredentials()
	var creds *google.Credentials

	// If custom project ID is provided, otherwise use the default project ID from the JSON key
	if projectId != nil {
		gcpServiceAccountCredentials.ProjectID = *projectId
	}

	if config.GCPTokenSource == "custom" {

		jsonKey, err := json.Marshal(gcpServiceAccountCredentials)
		if err != nil {
			logging.Fatal("failed to marshal GCP config: " + err.Error())
		}

		// Load the credentials from the JSON key
		creds, err = google.CredentialsFromJSON(ctx, jsonKey, config.CloudPlatformScope.String())
		if err != nil {
			logging.Fatal("failed to load GCP credentials from JSON key file: " + err.Error())
		}

	} else {
		// Load the default GCP credentials with Cloud Platform scope.
		creds, err = gcp.DefaultCredentials(context.Background())
		if err != nil {
			logging.Fatal("failed to load Default GCP credentials: " + err.Error())
		}
	}

	// Create a BigQuery client
	client, err = bigquery.NewClient(ctx, gcpServiceAccountCredentials.ProjectID, option.WithCredentials(creds))
	if err != nil {
		logging.Error(err.Error())
	}

	return
}
