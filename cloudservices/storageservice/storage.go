package storageservice

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"io"
	"os"
	"time"

	gcs "cloud.google.com/go/storage"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"gitlab.com/bobble-public/backend/go-utils/cloudservices/config"
	cloudserviceErrors "gitlab.com/bobble-public/backend/go-utils/cloudservices/errors"
	"gitlab.com/bobble-public/backend/go-utils/logging"
	"gocloud.dev/blob"
	"gocloud.dev/blob/gcsblob"
	"gocloud.dev/blob/s3blob"
	"gocloud.dev/gcp"
	"golang.org/x/oauth2/google"
)

// Storage represents the interface containing function declarations to operate on the cloud storage
type Storage interface {
	Upload(ctx context.Context, srcPath string, destPath string, uploadOptions *UploadOptions) (err error)
	Download(ctx context.Context, srcPath string, destPath string) (err error)
	Delete(ctx context.Context, path string) (err error)
	StreamFileDownload(ctx context.Context, srcPath string, batchSize int) <-chan string
	ListFiles(ctx context.Context, path string) ([]*blob.ListObject, error)
	SignedURL(ctx context.Context, path string, opts *SignedURLOptions) (url string, err error)
}

type storage struct {
	bucketName     string // Storage bucketName to upload
	handler        *blob.Bucket
	storageOptions *StorageOptions
}

// StorageOptions represents the options to create a new storage object
type StorageOptions struct {
	PermissionScope string
	// CloudProvider specifies the cloud provider for the storage service
	CloudProvider string
}

// SignedURLOptions sets options for SignedURL.
type SignedURLOptions struct {
	// Expiry sets how long the returned URL is valid for.
	// Defaults to DefaultSignedURLExpiry.
	Expiry time.Duration

	// Method is the HTTP method that can be used on the URL; one of "GET", "PUT",
	// or "DELETE". Defaults to "GET".
	Method string

	// ContentType specifies the Content-Type HTTP header the user agent is
	// permitted to use in the PUT request. It must match exactly. See
	// EnforceAbsentContentType for behavior when ContentType is the empty string.
	// If a bucket does not implement this verification, then it returns an
	// Unimplemented error.
	//
	// Must be empty for non-PUT requests.
	ContentType string

	// If EnforceAbsentContentType is true and ContentType is the empty string,
	// then PUTing to the signed URL will fail if the Content-Type header is
	// present. Not all buckets support this: ones that do not will return an
	// Unimplemented error.
	//
	// If EnforceAbsentContentType is false and ContentType is the empty string,
	// then PUTing without a Content-Type header will succeed, but it is
	// implementation-specific whether providing a Content-Type header will fail.
	//
	// Must be false for non-PUT requests.
	EnforceAbsentContentType bool
}

// UploadOptions represents the options to upload a file to the cloud storage
type UploadOptions struct {
	// fileContentsBuffer is the buffer containing the file contents to be uploaded to the cloud storage
	FileContentsBuffer *bytes.Buffer

	// ContentType specifies the MIME type of the blob being written. If not set,
	// it will be inferred from the content using the algorithm described at
	// http://mimesniff.spec.whatwg.org/.
	// https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Type
	ContentType string

	// Metadata holds key/value strings to be associated with the blob, or nil.
	// Keys may not be empty, and are lowercased before being written.
	// Duplicate case-insensitive keys (e.g., "foo" and "FOO") will result in
	// an error.
	Metadata map[string]string

	// CacheControl specifies caching attributes that services may use
	// when serving the blob.
	// https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control
	CacheControl string

	// PublicRead specifies whether the blob should be readable by all users.
	PublicRead bool
}

// NewStorage creates an object that represent the Storage interface
func NewStorage(ctx context.Context, bucketName string, projectId *string, storageOptions *StorageOptions) Storage {

	var handler *blob.Bucket
	var err error

	var cloudProvider = config.CloudProvider

	if storageOptions != nil && storageOptions.CloudProvider != "" {
		cloudProvider = storageOptions.CloudProvider
	}

	switch cloudProvider {
	case config.CloudProviderGCP:
		gcpServiceAccountCredentials := config.GetGCPServiceAccountCredentials()
		var creds *google.Credentials

		if config.GCPTokenSource == "custom" {

			// If custom project ID is provided, otherwise use the default project ID from the JSON key
			if projectId != nil {
				gcpServiceAccountCredentials.ProjectID = *projectId
			}

			jsonKey, err := json.Marshal(gcpServiceAccountCredentials)
			if err != nil {
				logging.Fatal("failed to marshal GCP config: " + err.Error())
			}

			// If custom permission scope is provided, otherwise use the default permission scope
			if storageOptions == nil {
				storageOptions = &StorageOptions{}
				storageOptions.PermissionScope = config.DevstorageReadWriteScope.String()
			}

			//Validate the permission scope
			if storageOptions.PermissionScope != "" && !config.PermissionScope(storageOptions.PermissionScope).IsValid() {
				logging.Fatal("invalid permission scope: " + storageOptions.PermissionScope)
			}

			// Load the credentials from the JSON key
			creds, err = google.CredentialsFromJSON(ctx, jsonKey, storageOptions.PermissionScope)
			if err != nil {
				logging.Fatal("failed to load GCP credentials from JSON key file: " + err.Error())
			}

		} else {
			// Load the default GCP credentials with Cloud Platform scope.
			creds, err = gcp.DefaultCredentials(context.Background())
			if err != nil {
				logging.Fatal("failed to load Default GCP credentials: " + err.Error())
			}
		}

		// Create a GCP storage client
		client, err := gcp.NewHTTPClient(gcp.DefaultTransport(), creds.TokenSource)
		if err != nil {
			logging.Fatal("failed to load GCP credentials from JSON key file: " + err.Error())
		}

		handler, err = gcsblob.OpenBucket(
			ctx,
			client,
			bucketName,
			&gcsblob.Options{
				GoogleAccessID: gcpServiceAccountCredentials.ClientEmail,
				PrivateKey:     []byte(gcpServiceAccountCredentials.PrivateKey),
			},
		)
		if err != nil {
			logging.Fatal(err.Error())
		}

	case config.CloudProviderAWS:
		// default to aws
		awsCredentials := config.GetAWSCredentials()
		awsAccessKeyID := awsCredentials.AccessKeyID
		awsSecretAccessKey := awsCredentials.SecretAccessKey
		awsRegion := awsCredentials.Region

		// create a new aws session
		awsSession, err := session.NewSession(&aws.Config{Region: aws.String(awsRegion), Credentials: credentials.NewStaticCredentials(awsAccessKeyID, awsSecretAccessKey, "")})
		if err != nil {
			logging.Fatal(err.Error())
		}
		handler, err = s3blob.OpenBucket(ctx, awsSession, bucketName, nil)
		if err != nil {
			logging.Fatal(err.Error())
		}

	default:
		logging.Fatal(cloudserviceErrors.ErrInvalidCloudProvider.Error())
	}
	if err != nil {
		return nil
	}

	return &storage{
		handler:        handler,
		bucketName:     bucketName,
		storageOptions: storageOptions,
	}
}

// Upload uploads the file to the cloud storage
func (storage *storage) Upload(ctx context.Context, srcPath string, destPath string, uploadOptions *UploadOptions) error {

	if uploadOptions == nil {
		uploadOptions = &UploadOptions{}
	}

	if uploadOptions.FileContentsBuffer != nil {
		tempFilePath, err := createTempFile(*uploadOptions.FileContentsBuffer, srcPath)
		if err != nil {
			return err
		}

		defer func(tempFilePath string) {
			err := deleteTempFile(tempFilePath)
			if err != nil {
				logging.Error(err.Error())
			}
		}(tempFilePath)

		srcPath = tempFilePath
	}

	// open the source file
	srcFile, err := os.Open(srcPath)
	if err != nil {
		return err
	}
	// close the src file when return
	defer func(srcFile *os.File) {
		err := srcFile.Close()
		if err != nil {
			logging.Error(err.Error())
		}
	}(srcFile)

	opts := blob.WriterOptions{}
	if storage.storageOptions == nil {
		storage.storageOptions = &StorageOptions{}
	}

	if uploadOptions.CacheControl != "" {
		opts.CacheControl = uploadOptions.CacheControl
	}

	if uploadOptions.Metadata != nil {
		opts.Metadata = uploadOptions.Metadata
	}

	if uploadOptions.ContentType != "" {
		opts.ContentType = uploadOptions.ContentType
	}

	// Set the ACL to all users can read
	opts.BeforeWrite = func(asFunc func(interface{}) bool) error {

		var putObjInput *s3manager.UploadInput
		var gcsWriter *gcs.Writer

		if asFunc(&putObjInput) {

			if uploadOptions.PublicRead {
				putObjInput.ACL = aws.String("public-read")
			}

		} else if asFunc(&gcsWriter) {

			if uploadOptions.PublicRead {
				gcsWriter.ObjectAttrs.ACL = []gcs.ACLRule{{Entity: gcs.AllUsers, Role: gcs.RoleReader}}
			}

		}

		return nil
	}

	// create a new storage object
	w, err := storage.handler.NewWriter(ctx, destPath, &opts)
	if err != nil {
		return err
	}

	// try to pot the object in storage
	if _, err := io.Copy(w, srcFile); err != nil {
		// if first try fails, retry
		if _, err := io.Copy(w, srcFile); err != nil {
			return err
		}
	}

	// Check that the write operation successfully completed
	err = w.Close()

	return err
}

// Download downloads the file from the cloud storage
func (storage *storage) Download(ctx context.Context, srcPath, destPath string) error {

	destFile, err := os.Create(destPath)
	if err != nil {
		return err
	}
	defer func(destFile *os.File) {
		err := destFile.Close()
		if err != nil {
			logging.Error(err.Error())
		}
	}(destFile)

	// create a reader to read data from the source path
	reader, err := storage.handler.NewReader(ctx, srcPath, nil)
	if err != nil {
		return err
	}

	defer func(reader *blob.Reader) {
		err := reader.Close()
		if err != nil {
			logging.Error(err.Error())
		}
	}(reader)

	// write data to the file
	_, err = io.Copy(destFile, reader)
	if err != nil {
		return err
	}

	return err
}

// Delete deletes the file from the cloud storage
func (storage *storage) Delete(ctx context.Context, path string) error {
	err := storage.handler.Delete(ctx, path)
	if err != nil {
		return err
	}
	return nil
}

// SignedURL returns a URL that can be used to GET (default), PUT or DELETE
// the blob for the duration specified in opts.Expiry.
//
// A nil SignedURLOptions is treated the same as the zero value.
//
// It is valid to call SignedURL for a key that does not exist.
//
// If the driver does not support this functionality, SignedURL
// will return an error for which gcerrors.Code will return gcerrors.Unimplemented.
func (storage *storage) SignedURL(ctx context.Context, path string, signedURLOptions *SignedURLOptions) (string, error) {
	if signedURLOptions == nil {
		signedURLOptions = new(SignedURLOptions)
	}

	dopts := new(blob.SignedURLOptions)
	dopts.Expiry = signedURLOptions.Expiry
	dopts.Method = signedURLOptions.Method
	dopts.ContentType = signedURLOptions.ContentType
	dopts.EnforceAbsentContentType = signedURLOptions.EnforceAbsentContentType

	url, err := storage.handler.SignedURL(ctx, path, dopts)
	if err != nil {
		return "", err
	}
	return url, nil
}


func (storage *storage) StreamFileDownload(ctx context.Context, srcPath string, batchSize int) <-chan string {
	resultChannel := make(chan string, batchSize) // Buffered channel with size 'batchSize'

	go func() {

		defer func() {
			close(resultChannel)
		}()

		// create a reader to read data from the source path
		reader, err := storage.handler.NewReader(ctx, srcPath, nil)
		if err != nil {
			logging.Error(err.Error())
			return
		}

		defer func() {
			err := reader.Close()
			if err != nil {
				logging.Error(err.Error())
			}
		}()

		scanner := bufio.NewScanner(reader)

		// Loop to read x number of lines at a time
		for scanner.Scan() {
			line := scanner.Text()
			// Send the line to the result channel
			resultChannel <- line
		}

		if err := scanner.Err(); err != nil {
			logging.Error(err.Error())
		}
	}()

	return resultChannel
}

// ListFiles lists the files in the given path
func (storage *storage) ListFiles(ctx context.Context, path string) ([]*blob.ListObject, error) {
	iter := storage.handler.List(&blob.ListOptions{
		Prefix: path,
	})

	files := make([]*blob.ListObject, 0)
	for {
		obj, err := iter.Next(ctx)
		if err == io.EOF {
			break
		}
		if err != nil {
			return files, err
		}
		files = append(files, obj)
	}

	return files, nil
}
