package storageservice

import (
	"bytes"
	"os"
	"path/filepath"

	"gitlab.com/bobble-public/backend/go-utils/logging"
)

func createTempFile(buf bytes.Buffer, filename string) (string, error) {
	// Create the directory if it doesn't exist
	tempDir := os.TempDir()
	uploadsDir := filepath.Join(tempDir, "uploads")
	_, err := os.Stat(uploadsDir)
	if os.IsNotExist(err) {
		err = os.MkdirAll(uploadsDir, os.ModePerm)
		if err != nil {
			return "", err
		}
	} else if err != nil {
		return "", err
	}
	// Create a new file in the temp directory
	tempFilePath := filepath.Join(uploadsDir, filename)
	file, err := os.Create(tempFilePath)
	if err != nil {
		return "", err
	}

	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			logging.Error(err.Error())
		}
	}(file)

	// Write the buffer to the file
	_, err = buf.WriteTo(file)
	if err != nil {
		return "", err
	}

	return tempFilePath, nil
}

func deleteTempFile(fileName string) error {

	_, err := os.Stat(fileName)
	if os.IsNotExist(err) {
		return nil
	} else if err != nil {
		return err
	}

	err = os.Remove(fileName)
	if err != nil {
		return err
	}
	return nil
}
