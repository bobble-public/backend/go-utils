package echologging

import (
	"bytes"
	"encoding/json"
	"io"
	"os"
	"time"

	"github.com/labstack/echo/v4"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// NewEchoMiddleware returns a new echo middleware that logs requests.
func NewEchoMiddleware(levelStr string) (echo.MiddlewareFunc, error) {
	// Set default level to error.
	level := zapcore.ErrorLevel
	if levelStr != "" {
		parsedLevel, err := zapcore.ParseLevel(levelStr)
		if err != nil {
			return nil, err
		}
		level = parsedLevel
	}

	// Define a custom time format.
	encoderCfg := zap.NewProductionEncoderConfig()
	encoderCfg.EncodeTime = func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
		enc.AppendString(t.UTC().Format(time.RFC3339))
	}
	atomInstance := zap.NewAtomicLevelAt(level)
	loggerInstance := zap.New(zapcore.NewCore(
		zapcore.NewJSONEncoder(encoderCfg),
		zapcore.Lock(os.Stdout),
		atomInstance,
	), zap.WithCaller(false), zap.AddStacktrace(zap.PanicLevel))

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			start := time.Now()

			// Read the original request body
			originalRequestBody, err := io.ReadAll(c.Request().Body)
			if err != nil {
				c.Error(err)
				loggerInstance.With(zap.Error(err)).Error("Error Reading Request Body in Echo Logging Middleware")
			}

			// Create a new buffer to store a copy of the request body
			var requestBodyCopy bytes.Buffer
			_, err = requestBodyCopy.Write(originalRequestBody)
			if err != nil {
				c.Error(err)
				loggerInstance.With(zap.Error(err)).Error("Error Copying Request Body in Echo Logging Middleware")
			}

			// Replace the request body with the copy
			c.Request().Body = io.NopCloser(&requestBodyCopy)

			err = next(c)
			if err != nil {
				c.Error(err)
			}

			var bodyString string
			if err == nil {
				// Parse the request body into a map to log it as JSON.
				var requestBody map[string]interface{}
				if jsonErr := json.Unmarshal(originalRequestBody, &requestBody); jsonErr == nil {
					// Marshal the map into minified JSON.
					minifiedJSON, jsonErr := json.Marshal(requestBody)
					if jsonErr == nil {
						bodyString = string(minifiedJSON)
					}
				}
			}

			res := c.Response()

			fields := []zapcore.Field{
				zap.String("method", c.Request().Method),
				zap.Int("status", c.Response().Status),
				zap.String("url", c.Request().URL.Path),
				zap.String("remote_addr", c.Request().RemoteAddr),
				zap.String("remote_ip", c.RealIP()),
				zap.String("host", c.Request().Host),
				zap.String("user_agent", c.Request().UserAgent()),
				zap.String("latency", time.Since(start).String()),
				zap.Int64("bytes_out", c.Response().Size),
				zap.Int64("bytes_in", c.Request().ContentLength),
				zap.String("query", c.Request().URL.RawQuery),
				zap.String("body", bodyString),
			}

			n := res.Status
			switch {
			case n >= 500:
				loggerInstance.With(zap.Error(err)).Error("server error", fields...)
			case n >= 400:
				loggerInstance.With(zap.Error(err)).Warn("client error", fields...)
			case n >= 300:
				loggerInstance.Info("redirection", fields...)
			default:
				loggerInstance.Info("success", fields...)
			}

			return nil
		}
	}, nil
}
