package logging

// Info logs a message with severity INFO.
func Info(message string) {
	logger.Info(message)
}

// Debug logs a message with severity DEBUG.
func Debug(message string) {
	logger.Debug(message)
}

// Warn logs a message with severity WARNING.
func Warn(message string) {
	logger.Warn(message)
}

// Error logs a message with severity ERROR.
func Error(message string) {
	logger.Error(message)
}

// Panic logs a message with severity PANIC.
func Panic(message string) {
	logger.Panic(message)
}

// Fatal logs a message with severity FATAL.
func Fatal(message string) {
	logger.Fatal(message)
}
