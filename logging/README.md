# GO UTILS LOGGING PACKAGE

This package is a wrapper around zap logging package, which provides simple config free global logging support along with structured logging.

## Log Levels

```text
debug -> info -> warn -> error -> panic -> fatal
```

## Example

```go
package main

import (
	"errors"
	"gitlab.com/bobble-public/backend/go-utils/logging"
)

func main() {
	// setting level
	err := logging.SetLevel("error")
	if err != nil {
		panic(err)
	}
	// global logger usage
	logging.Debug("debug log")
	logging.Info("info log")
	logging.Warn("warn")
	logging.Error("error log")

	// global logger with fields usage
	logging.WithFields(map[string]interface{}{
		"data":  "value",
		"data2": "value2",
		"err":   errors.New("some error"),
	}).Info("info log")
}
```