package logging

// Fields is a map of fields to add to a log message.
type Fields map[string]interface{}

// WithFields returns a new Fields instance with the given fields.
func WithFields(fields Fields) Logging {
	return &fields
}

func (fields *Fields) internal() []interface{} {
	internal := make([]interface{}, 0)
	for key, value := range *fields {
		internal = append(internal, key, value)
	}

	return internal
}

// Info logs a message with severity INFO.
func (fields *Fields) Info(message string) {
	sugared.Infow(message, fields.internal()...)
}

// Debug logs a message with severity DEBUG.
func (fields *Fields) Debug(message string) {
	sugared.Debugw(message, fields.internal()...)
}

// Warn logs a message with severity WARNING.
func (fields *Fields) Warn(message string) {
	sugared.Warnw(message, fields.internal()...)
}

// Error logs a message with severity ERROR.
func (fields *Fields) Error(message string) {
	sugared.Errorw(message, fields.internal()...)
}

// Panic logs a message with severity PANIC.
func (fields *Fields) Panic(message string) {
	sugared.Panicw(message, fields.internal()...)
}

// Fatal logs a message with severity FATAL.
func (fields *Fields) Fatal(message string) {
	sugared.Fatalw(message, fields.internal()...)
}
