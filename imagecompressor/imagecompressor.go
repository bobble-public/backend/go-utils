package imagecompressor

import (
	"bytes"
	"fmt"
	"os/exec"
	"strings"
)

type ImageCompressOptions struct {
	TargetHeight int  // Target height for resizing
	TargetWidth  int  // Target width for resizing
	Quality      int  // Compression quality (default minimum: 40)
	Lossless     bool // Enable lossless compression
}

// CompressPng uses pngquant to compress the PNG image in inputBuffer.
// The quality range is between 0 and 100, with minQuality <= maxQuality.
// minQuality is the lowest acceptable quality (%), and maxQuality is the highest (%) (recommended 60-80).
// It returns a new *bytes.Buffer with the compressed image.
func CompressPng(inputBuffer *bytes.Buffer, minQuality, maxQuality int) (*bytes.Buffer, error) {
	// Ensure quality range is valid (minQuality <= maxQuality)
	if minQuality < 0 || maxQuality > 100 || minQuality > maxQuality {
		return nil, fmt.Errorf("invalid quality range: %d-%d. Accepted Quality range is between 0 and 100, with minQuality <= maxQuality", minQuality, maxQuality)
	}

	// Build the quality argument string
	qualityArg := fmt.Sprintf("--quality=%d-%d", minQuality, maxQuality)

	// Create an exec.Command to run pngquant with required options
	cmd := exec.Command("pngquant", qualityArg, "--speed=1", "-")

	// Create a pipe for the input buffer (the PNG file content)
	cmd.Stdin = inputBuffer

	// Get the output buffer where pngquant will write the compressed image
	var outputBuffer bytes.Buffer
	cmd.Stdout = &outputBuffer

	// Run the command and check for errors
	err := cmd.Run()
	if err != nil {
		// Check if the error is related to pngquant not being installed
		if strings.Contains(err.Error(), "executable file not found") {
			return nil, fmt.Errorf("pngquant is not installed. Please install pngquant and try again")
		}
		// Return the original error if it's not related to pngquant not being found
		return nil, fmt.Errorf("pngquant failed: %w", err)
	}

	// Return the compressed image as a *bytes.Buffer
	return &outputBuffer, nil
}

// CompressWebP compresses a WebP file using ImageMagick with input and output as bytes.Buffer.
// The original input buffer remains unaltered.
func CompressWebP(inputBuffer *bytes.Buffer, options ImageCompressOptions) (*bytes.Buffer, error) {
	// Create a copy of the input buffer to ensure the original buffer is unaltered
	copiedBuffer := bytes.NewBuffer(inputBuffer.Bytes())

	// Create an output buffer to store the compressed image
	outputBuffer := new(bytes.Buffer)

	// Ensure quality is at least 40
	if options.Quality < 40 {
		options.Quality = 40
	}

	// Define ImageMagick `convert` command arguments
	args := []string{
		"webp:-",              // Input format from stdin
		"-layers", "coalesce", // Handle animated WebP
		"-quality", fmt.Sprintf("%d", options.Quality), // Set compression quality
	}

	// Add resize only if height and width are provided
	if options.TargetHeight > 0 && options.TargetWidth > 0 {
		args = append(args, "-resize", fmt.Sprintf("%dx%d", options.TargetWidth, options.TargetHeight))
	}

	// Enable lossless compression if specified
	if options.Lossless {
		args = append(args, "-define", "webp:lossless=true")
	}

	args = append(args, "-loop", "0") // Preserve animation loop count
	args = append(args, "webp:-")     // Output format to stdout

	// Execute the ImageMagick command
	cmd := exec.Command("convert", args...)

	// Assign the copied input buffer and output buffers
	cmd.Stdin = copiedBuffer  // Input data as copied buffer
	cmd.Stdout = outputBuffer // Output goes into bytes.Buffer

	// Capture stderr for debugging
	var stderr bytes.Buffer
	cmd.Stderr = &stderr

	// Run the command
	err := cmd.Run()
	if err != nil {
		return nil, fmt.Errorf("compression failed: %w\nStderr: %s", err, stderr.String())
	}

	return outputBuffer, nil
}

func CompressJPEG(inputBuffer *bytes.Buffer, options ImageCompressOptions) (*bytes.Buffer, error) {

	// Create a copy of the input buffer to ensure the original buffer is unaltered
	copiedBuffer := bytes.NewBuffer(inputBuffer.Bytes())

	// Create an output buffer to store the compressed image
	outputBuffer := new(bytes.Buffer)

	// Ensure quality is at least 40
	if options.Quality < 40 {
		options.Quality = 40
	}

	// Define ImageMagick `convert` command arguments
	args := []string{
		"jpeg:-",                                       // Input format from stdin
		"-quality", fmt.Sprintf("%d", options.Quality), // Set compression quality
	}

	// Add resize only if height and width are provided
	if options.TargetHeight > 0 && options.TargetWidth > 0 {
		args = append(args, "-resize", fmt.Sprintf("%dx%d", options.TargetWidth, options.TargetHeight))
	}

	args = append(args, "jpeg:-") // Output format to stdout

	// Execute the ImageMagick command
	cmd := exec.Command("convert", args...)

	// Assign input and output buffers
	cmd.Stdin = copiedBuffer
	cmd.Stdout = outputBuffer

	// Capture stderr for debugging
	var stderr bytes.Buffer
	cmd.Stderr = &stderr

	// Run the command
	err := cmd.Run()
	if err != nil {
		return nil, fmt.Errorf("JPEG compression failed: %w\nStderr: %s", err, stderr.String())
	}

	return outputBuffer, nil
}

func CompressGIF(inputBuffer *bytes.Buffer, options ImageCompressOptions) (*bytes.Buffer, error) {

	// Create a copy of the input buffer to ensure the original buffer is unaltered
	copiedBuffer := bytes.NewBuffer(inputBuffer.Bytes())

	// Create an output buffer to store the compressed image
	outputBuffer := new(bytes.Buffer)

	// Define ImageMagick `convert` command arguments
	args := []string{
		"gif:-", // Input format from stdin
	}

	// Add resize only if height and width are provided
	if options.TargetHeight > 0 && options.TargetWidth > 0 {
		args = append(args, "-resize", fmt.Sprintf("%dx%d", options.TargetWidth, options.TargetHeight))
	}

	// Optimize animated GIF
	args = append(args, "-layers", "optimize")

	args = append(args, "gif:-") // Output format to stdout

	// Execute the ImageMagick command
	cmd := exec.Command("convert", args...)

	// Assign input and output buffers
	cmd.Stdin = copiedBuffer
	cmd.Stdout = outputBuffer

	// Capture stderr for debugging
	var stderr bytes.Buffer
	cmd.Stderr = &stderr

	// Run the command
	err := cmd.Run()
	if err != nil {
		return nil, fmt.Errorf("GIF compression failed: %w\nStderr: %s", err, stderr.String())
	}

	return outputBuffer, nil
}
