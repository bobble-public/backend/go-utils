package promutil

import (
	"sync"

	"github.com/prometheus/client_golang/prometheus"
)

var queueBufferRecorderInstance *queueBufferRecorder
var queueBufferRecorderSingleton sync.Once

type queueBufferRecorder struct {
	count *prometheus.GaugeVec
}

type QueueBufferRecorder interface {
	Record(name string, latency float64)
}

func GetQueueBufferRecorder() QueueBufferRecorder {
	queueBufferRecorderSingleton.Do(func() {
		count := prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Name: "queue_buffer_count",
			}, []string{"name"})

		prometheus.MustRegister(count)

		queueBufferRecorderInstance = &queueBufferRecorder{
			count: count,
		}
	})

	return queueBufferRecorderInstance
}

func (queueBufferRecorder *queueBufferRecorder) Record(name string, count float64) {
	queueBufferRecorder.count.WithLabelValues(name).Set(count)
}
