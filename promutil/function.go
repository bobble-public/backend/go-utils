package promutil

import (
	"sync"

	"github.com/prometheus/client_golang/prometheus"
)

var functionRecorderInstance *functionRecorder
var functionRecorderSingleton sync.Once

type functionRecorder struct {
	latency *prometheus.HistogramVec
	count   *prometheus.CounterVec
}

type FunctionRecorder interface {
	Record(name string, status string, latency float64)
}

func GetFunctionRecorder() FunctionRecorder {
	functionRecorderSingleton.Do(func() {
		latency := prometheus.NewHistogramVec(
			prometheus.HistogramOpts{
				Name:    "function_duration_seconds",
				Buckets: DefBuckets,
			}, []string{"name", "status"})
		count := prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: "function_calls_total",
			}, []string{"name", "status"})

		prometheus.MustRegister(latency)
		prometheus.MustRegister(count)

		functionRecorderInstance = &functionRecorder{
			latency: latency,
			count:   count,
		}
	})

	return functionRecorderInstance
}

func (functionRecorder *functionRecorder) Record(name string, status string, latency float64) {
	functionRecorder.count.WithLabelValues(name, status).Inc()
	functionRecorder.latency.WithLabelValues(name, status).Observe(latency)
}
