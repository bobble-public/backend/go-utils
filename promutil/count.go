package promutil

import (
	"sync"

	"github.com/prometheus/client_golang/prometheus"
)

var countRecorderInstance *countRecorder
var countRecorderSingleton sync.Once

type countRecorder struct {
	count     *prometheus.CounterVec
	frequency *prometheus.CounterVec
}

type CountRecorder interface {
	Record(key string, count int)
}

func GetCountRecorder() CountRecorder {
	countRecorderSingleton.Do(func() {
		count := prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: "count_calls_total",
				Help: "Total number of count calls.",
			}, []string{"key"})

		frequency := prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: "count_function_calls_total",
				Help: "Total number of function calls.",
			}, []string{"key"})

		prometheus.MustRegister(count)
		prometheus.MustRegister(frequency)

		countRecorderInstance = &countRecorder{
			count:     count,
			frequency: frequency,
		}
	})

	return countRecorderInstance
}

func (recorder *countRecorder) Record(key string, count int) {
	recorder.count.WithLabelValues(key).Add(float64(count))
	recorder.frequency.WithLabelValues(key).Inc()
}
