package promutil

import (
	"sync"

	"github.com/prometheus/client_golang/prometheus"
)

var externalHTTPRecorderInstance *externalHTTPRecorder
var externalHTTPRecorderSingleton sync.Once

type externalHTTPRecorder struct {
	latency *prometheus.HistogramVec
	count   *prometheus.CounterVec
}

type ExternalHTTPRecorder interface {
	Record(url string, method string, status_code string, latency float64)
}

func GetExternalHTTPRecorder() ExternalHTTPRecorder {
	externalHTTPRecorderSingleton.Do(func() {
		latency := prometheus.NewHistogramVec(
			prometheus.HistogramOpts{
				Name:    "external_http_duration_seconds",
				Buckets: DefBuckets,
			}, []string{"url", "method", "status_code"})
		count := prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: "external_http_calls_total",
			}, []string{"url", "method", "status_code"})

		prometheus.MustRegister(latency)
		prometheus.MustRegister(count)

		externalHTTPRecorderInstance = &externalHTTPRecorder{
			latency: latency,
			count:   count,
		}
	})

	return externalHTTPRecorderInstance
}

func (externalHTTPRecorder *externalHTTPRecorder) Record(url string, method string, status_code string, latency float64) {
	externalHTTPRecorder.count.WithLabelValues(url, method, status_code).Inc()
	externalHTTPRecorder.latency.WithLabelValues(url, method, status_code).Observe(latency)
}
