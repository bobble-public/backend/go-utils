package promutil

import (
	"sync"

	"github.com/prometheus/client_golang/prometheus"
)

var errorRecorderInstance *errorRecorder
var errorRecorderSingleton sync.Once

type errorRecorder struct {
	count   *prometheus.CounterVec
}

type ErrorRecorder interface {
	Record(method string, code string, key string)
}

func GetErrorRecorder() ErrorRecorder {
	errorRecorderSingleton.Do(func() {
		count := prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: "errors_total",
			}, []string{"method", "code", "key"})

		prometheus.MustRegister(count)

		errorRecorderInstance = &errorRecorder{
			count:   count,
		}
	})

	return errorRecorderInstance
}

func (errorRecorder *errorRecorder) Record(method string, code string, key string) {
	errorRecorder.count.WithLabelValues(method, code, key).Inc()
}
