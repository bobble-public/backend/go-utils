package promutil

import (
	"sync"

	"github.com/prometheus/client_golang/prometheus"
)

var queueLatencyRecorderInstance *queueLatencyRecorder
var queueLatencyRecorderSingleton sync.Once

type queueLatencyRecorder struct {
	e2eLatency *prometheus.HistogramVec
	p2cLatency *prometheus.HistogramVec
}

type QueueLatencyRecorder interface {
	RecordE2ELatency(name string, e2eLatency float64)
	RecordP2CLatency(name string, p2cLatency float64)
}

func GetQueueLatencyRecorder() QueueLatencyRecorder {
	queueLatencyRecorderSingleton.Do(func() {
		e2eLatency := prometheus.NewHistogramVec(
			prometheus.HistogramOpts{
				Name:    "queue_e2e_duration_seconds",
				Buckets: DefBuckets,
			}, []string{"name"})
		p2cLatency := prometheus.NewHistogramVec(
			prometheus.HistogramOpts{
				Name:    "queue_p2c_duration_seconds",
				Buckets: DefBuckets,
			}, []string{"name"})

		prometheus.MustRegister(e2eLatency)
		prometheus.MustRegister(p2cLatency)

		queueLatencyRecorderInstance = &queueLatencyRecorder{
			e2eLatency: e2eLatency,
			p2cLatency: p2cLatency,
		}
	})

	return queueLatencyRecorderInstance
}

func (queueLatencyRecorder *queueLatencyRecorder) RecordE2ELatency(name string, e2eLatency float64) {
	queueLatencyRecorder.e2eLatency.WithLabelValues(name).Observe(e2eLatency)
}

func (queueLatencyRecorder *queueLatencyRecorder) RecordP2CLatency(name string, p2cLatency float64) {
	queueLatencyRecorder.p2cLatency.WithLabelValues(name).Observe(p2cLatency)
}
