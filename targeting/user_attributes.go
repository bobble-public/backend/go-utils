package targeting

import (
	"encoding/json"
	"fmt"
)

// Define the UserMetadata struct
type UserMetadata struct {
	PremiumUserMetadata PremiumUserMetadata `json:"premiumUserMetadata"`
	Segments            []int               `json:"segments"`
	PackageName         string              `json:"packageName"`
}

// Define the PremiumUserMetadata struct
type PremiumUserMetadata struct {
	IsPremiumUser          bool                        `json:"isPremiumUser"`
	RecommendedProducts    []PremiumRecommendedProduct `json:"recommendedProducts"`
	PurchasedProducts      []PremiumPurchasedProduct   `json:"purchasedProducts"`
	PremiumStickersDetails PremiumStickersDetails      `json:"premiumStickersDetails"`
}

type PremiumRecommendedProduct struct {
	ProductId                   int                         `json:"productId"`
	GenAITriggerDetails         GenAITriggerDetails         `json:"genAITriggerDetails"`
	KeyboardThemeTriggerDetails KeyboardThemeTriggerDetails `json:"keyboardThemeTriggerDetails"`
}

type PremiumPurchasedProduct struct {
	ProductId                   int                         `json:"productId"`
	GenAITriggerDetails         GenAITriggerDetails         `json:"genAITriggerDetails"`
	KeyboardThemeTriggerDetails KeyboardThemeTriggerDetails `json:"keyboardThemeTriggerDetails"`
}

type GenAITriggerDetails struct {
	Deeplink string `json:"deeplink"`
}

type KeyboardThemeTriggerDetails struct {
	Deeplink string `json:"deeplink"`
}

type PremiumStickersDetails struct {
	NumGeneratedSticker int `json:"numGeneratedStickers"`
}

// Define the generic Filter interface
type UserAttributeFilter interface {
	Matches(userMeta UserMetadata) bool
}

// Define PremiumRecommendedProductFilter
type PremiumRecommendedProductFilter struct {
	ProductIDs []int `json:"productIds"`
}

func (f PremiumRecommendedProductFilter) Matches(userMeta UserMetadata) bool {
	for _, id := range f.ProductIDs {
		for _, recommended := range userMeta.PremiumUserMetadata.RecommendedProducts {
			if id == recommended.ProductId {
				return true
			}
		}
	}
	return false
}

// Define PremiumPurchasedProductFilter
type PremiumPurchasedProductFilter struct {
	ProductIDs []int `json:"productIds"`
}

func (f PremiumPurchasedProductFilter) Matches(userMeta UserMetadata) bool {
	for _, id := range f.ProductIDs {
		for _, purchased := range userMeta.PremiumUserMetadata.PurchasedProducts {
			if id == purchased.ProductId {
				return true
			}
		}
	}
	return false
}

// Define PremiumStickersFilter
type PremiumStickersFilter struct {
	MinNumStickers *int `json:"min_num_stickers"`
	MaxNumStickers *int `json:"max_num_stickers"`
}

func (f PremiumStickersFilter) Matches(userMeta UserMetadata) bool {
	numStickers := userMeta.PremiumUserMetadata.PremiumStickersDetails.NumGeneratedSticker
	if f.MinNumStickers != nil && numStickers < *f.MinNumStickers {
		return false
	}
	if f.MaxNumStickers != nil && numStickers > *f.MaxNumStickers {
		return false
	}
	return true
}

// Define PremiumUserFilter
type PremiumUserFilter struct {
	State bool `json:"state"`
}

func (f PremiumUserFilter) Matches(userMeta UserMetadata) bool {
	return userMeta.PremiumUserMetadata.IsPremiumUser == f.State
}

// Define SegmentFilter
type SegmentFilter struct {
	SegmentIDs []int `json:"segmentIds"`
}

func (f SegmentFilter) Matches(userMeta UserMetadata) bool {
	for _, segmentID := range f.SegmentIDs {
		for _, userSegment := range userMeta.Segments {
			if segmentID == userSegment {
				return true
			}
		}
	}
	return false
}

// Define Rule which contains multiple filters
type UserAttributeRule struct {
	Filters []UserAttributeFilter `json:"filters"`
}

// Implement a custom unmarshaler for the Rule struct
func (r *UserAttributeRule) UnmarshalJSON(data []byte) error {
	var rawFilters []struct {
		Type   string          `json:"type"`
		Config json.RawMessage `json:"config"`
	}

	if err := json.Unmarshal(data, &rawFilters); err != nil {
		return err
	}

	for _, rawFilter := range rawFilters {
		var filter UserAttributeFilter

		switch rawFilter.Type {
		case "flt_premium_recommended_product":
			var recommendedProductFilter PremiumRecommendedProductFilter
			if err := json.Unmarshal(rawFilter.Config, &recommendedProductFilter); err != nil {
				return err
			}
			filter = recommendedProductFilter
		case "flt_premium_purchased_product":
			var purchasedProductFilter PremiumPurchasedProductFilter
			if err := json.Unmarshal(rawFilter.Config, &purchasedProductFilter); err != nil {
				return err
			}
			filter = purchasedProductFilter
		case "flt_premium_stickers":
			var stickersFilter PremiumStickersFilter
			if err := json.Unmarshal(rawFilter.Config, &stickersFilter); err != nil {
				return err
			}
			filter = stickersFilter
		case "flt_premium_user":
			var premiumUserFilter PremiumUserFilter
			if err := json.Unmarshal(rawFilter.Config, &premiumUserFilter); err != nil {
				return err
			}
			filter = premiumUserFilter
		case "flt_segments":
			var segmentFilter SegmentFilter
			if err := json.Unmarshal(rawFilter.Config, &segmentFilter); err != nil {
				return err
			}
			filter = segmentFilter
		case "flt_disallowed_package":
			var disallowedPackageFilter DisallowedPackageFilter
			if err := json.Unmarshal(rawFilter.Config, &disallowedPackageFilter); err != nil {
				return err
			}
			filter = disallowedPackageFilter
		default:
			return fmt.Errorf("unknown filter type: %s", rawFilter.Type)
		}

		r.Filters = append(r.Filters, filter)
	}
	return nil
}

// Function to match a rule against criteria
func (r UserAttributeRule) Matches(userMeta UserMetadata) bool {
	for _, filter := range r.Filters {
		if !filter.Matches(userMeta) {
			return false
		}
	}
	return true
}

// UserAttributeRuleSet is responsible for loading rules and evaluating them against user metadata
type UserAttributeRuleSet struct {
	Rules []UserAttributeRule
}

// LoadRules loads rules from a JSON array
func (ruleSets *UserAttributeRuleSet) LoadRules(jsonRules []string) error {
	rules := make([]UserAttributeRule, 0, len(jsonRules))
	for _, jsonRule := range jsonRules {
		var rule UserAttributeRule
		if err := json.Unmarshal([]byte(jsonRule), &rule); err != nil {
			return fmt.Errorf("error decoding rule: %v", err)
		}
		rules = append(rules, rule)
	}
	ruleSets.Rules = rules
	return nil
}

// Evaluate matches the user metadata against the loaded rules
func (ruleSets *UserAttributeRuleSet) Evaluate(userMeta UserMetadata) (bool, error) {
	if len(ruleSets.Rules) == 0 {
		return true, nil // Return true if there are no rules to evaluate against
	}

	for _, rule := range ruleSets.Rules {
		if rule.Matches(userMeta) {
			return true, nil
		}
	}
	return false, nil
}

// Define DisallowedPackageFilter
type DisallowedPackageFilter struct {
	PackageNames []string `json:"packageNames"`
}

func (f DisallowedPackageFilter) Matches(userMeta UserMetadata) bool {
	for _, packageName := range f.PackageNames {
		if packageName == userMeta.PackageName {
			return false
		}
	}
	return true
}
